function listGifts(letter) {
  
    let gifts = letter.split(" ")
    gifts = gifts.filter(gift => !gift.includes("_"))
  
    let cleanGifts = [...new Set(gifts)]
    
    let final = cleanGifts.map((gift)=>{
      let counter = 0
      gifts.forEach((gift2)=> gift == gift2 && counter++)
      return [gift, counter]
    })
    
    return Object.fromEntries(final)
}