<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDishSelectedByEmployeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dish_selected_by_employee', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("id_employe");
            $table->unsignedBigInteger("id_dished_menu");
            $table->date("day");

            $table->foreign("id_employe")->references("id")->on("employee");
            $table->foreign("id_dished_menu")->references("id")->on("menu_dishes")->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dish_selected_by_employee');
    }
}
