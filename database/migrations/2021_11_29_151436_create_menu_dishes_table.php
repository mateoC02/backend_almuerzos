<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenuDishesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_dishes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("id_menu");
            $table->unsignedBigInteger("id_dished");
            $table->foreign("id_menu")  ->references("id")->on("menus")->cascadeOnDelete();
            $table->foreign("id_dished")->references("id")->on("disheds")->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_dishes');
    }
}
