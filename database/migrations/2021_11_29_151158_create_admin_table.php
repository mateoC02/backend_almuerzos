<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateAdminTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin', function (Blueprint $table) {
            $table->id();
            $table->string("username");
            $table->string("password");
            $table->unsignedBigInteger("id_role")->default(1);
            $table->string("status")->default(1);
            $table->foreign("id_role")->references("id")->on("roles");

        });

        DB::table("admin")->insert([
            [
                "username" => 'admin',
                "password" => '$2y$10$VhPjB4bZl5CuUUU0etqy8uQoTKUYXY.jN1yif5ulGxx7G/kfCY8Ty',
                "status"   => '1'
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin');
    }
}
