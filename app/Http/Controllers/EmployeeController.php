<?php

namespace App\Http\Controllers;

use App\Helpers\DatesHelper;
use App\Http\Messages\Messages;
use App\Models\EmployeeModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class EmployeeController extends BaseController
{
    public function index(Request $request){
        $user = $request->user;
        $this->sendSuccessResponse($user);
    }

    public function getMenu(Request $request, $date = null){
        $user = $request->user;

        if ( $date == null ){
            $weeks    = DatesHelper::getWeekByDay(date('Y-m-d'));
            $tomorrow = DatesHelper::getDateTomorrow($weeks[6][0]);
            $weeks    = DatesHelper::getWeekByDay($tomorrow);

            return $this->sendSuccessResponse(EmployeeModel::getMenuByDate($weeks[0],$weeks[5],$user["id"]) ?: []);
        }
        
        if (strlen($date) < 10 or !DatesHelper::existDate($date)) {
            $message = Messages::errorMessage("DATE_NOT_EXIST");
            return $this->sendErrorResponse($message,$message["http_error"]);
        }

        $dished = EmployeeModel::getSelectDishedMenu($date,$user["id"]);

        if(!empty($dished)){
            $message = Messages::successMessage("SELECT_DISHED");
            $message["data"] = $dished;
            return $this->sendSuccessResponse($message,200);   
        }
    
        return $this->sendErrorResponse(Messages::errorMessage("REGISTER_NOT_FOUND"),404);
    }

    public function getSelecteds(Request $request){

        $user = $request->user;
        
        $weeks    = DatesHelper::getWeekByDay(date('Y-m-d'));
        $tomorrow = DatesHelper::getDateTomorrow($weeks[6][0]);
        $weeks    = DatesHelper::getWeekByDay($tomorrow);
        
        $dishedsSelecteds = [];
        
        foreach($weeks as $date){
            $selecteds = EmployeeModel::getSelectedByDate(id: $user["id"],date: $date[0]);
            $selecteds = count($selecteds) == 0 ? ["dia" => "", "nombre" => ""] : $selecteds[0];
            array_push($dishedsSelecteds,$selecteds);
        }
        
        $message = Messages::successMessage("SUCCESS_REQUEST");
        $message["data"] = $dishedsSelecteds;
        return $this->sendSuccessResponse($message);
    }

    public function selectDishedMenu(Request $request){
        
        $body = $request->all();
        $user = $request->user;

        $validation = Validator::make($body ?: [], [
            'dia'                 => "required|min:10|max:10",  
            'id_plato_lista_menu' => "required|min:1"
        ]);

        
        if($validation->fails()){
            $error = Messages::errorMessage('REQUEST_BODY_FAIL');
            return $this->sendErrorResponse($error,$error["http_error"]);
        }

        if (!DatesHelper::existDate($body["dia"])) {
            $message = Messages::errorMessage("DATE_NOT_EXIST");
            return $this->sendErrorResponse($message,$message["http_error"]);
        }

        $dates = EmployeeModel::getDateMenuByIdDishedInMenu($body["id_plato_lista_menu"]);

        if(count($dates) == 0){
            $message = Messages::errorMessage("ERROR_REGISTER");
            $message["error_message"] = "no existe un plato en el menu con ese id";
            return $this->sendErrorResponse($message,$message["http_error"]);
        }

        $weeks = DatesHelper::getWeekByDay($body["dia"]);

        $dates = json_decode(json_encode($dates[0]),true);

        if(!($weeks[0][0] == $dates["start_date"]) AND !($weeks[6][0] == $dates["end_date"])){
            $message = Messages::errorMessage("ERROR_REGISTER");
            $message["error_message"] = "la fecha no esta dentro del rango de dias del menu";
            return $this->sendErrorResponse($message,500);
        }

        if(EmployeeModel::dateIsSelected($body["dia"],$user["id"])){
            $message = Messages::errorMessage("ERROR_REGISTER");
            $message["error_message"] = "ya existe una seleccion para el dia {$body['dia']}";
            return $this->sendErrorResponse($message,500);
        }

        $result = EmployeeModel::selectDishedMenu([
            "id_employe"     => $user["id"],
            "id_dished_menu" => $body["id_plato_lista_menu"],
            "day"            => $body["dia"]
        ]);
            
        if (is_null($result) or count($result) == 0 ) {
            $message = Messages::errorMessage("ERROR_REGISTER");
            $message["error_message"] = "ocurrio un error al momento de registrar el plato seleccionado";
            return $this->sendErrorResponse($message,500);
        }

        $message = Messages::successMessage("SUCCESS_REQUEST");
        $message["data"] =  $result[0];
        return $this->sendSuccessResponse($message);
    }
}
