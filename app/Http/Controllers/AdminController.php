<?php

namespace App\Http\Controllers;

use App\Helpers\DatesHelper;
use App\Http\Messages\Messages;
use App\Models\AdminModel;
use App\Models\DishedModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class AdminController extends BaseController{

    private $currentAdmin = [];

    public function index(Request $request){
        $user = $request->user;
        return $this->sendSuccessResponse($user);
    }

    public function getDashboard(){
        return $this->sendSuccessResponse([
            "platos" => DB::table("disheds")->count(),
            "menus"  => DB::table("menus")->count(),
            "admin"  => DB::table("admin")->count(),
        ]);
    }

    // =================================================
    //                    DISHEDS                     ||
    // ================================================ 

    public function getDished(Request $request,$find = null){
        
        $success = Messages::successMessage("SUCCESS_REQUEST");
        
        if($request->query("name")){
            $search = $request->query("name");
            $success["data"] = DishedModel::where('name','like','%' . $search . '%')->get();
            return $success;
        }

        if (is_null($find)) {
            $success["data"] = DishedModel::all();
            return $success;
        }

        if (is_numeric($find)) {
            $success["data"] = DishedModel::where('id',$find)->get();
            return $success;
        }
        
    }

    public function createDished(Request $request){

        $body = $request->all();
    
        $validation = Validator::make($body ?: [], [
            'platos'               => "required|array",
            'platos.*.name'        => 'required|min:1|max:150',
            'platos.*.description' => 'max:200'
        ]);

        if($validation->fails()){
            $error = Messages::errorMessage('REQUEST_BODY_FAIL');
            return $this->sendErrorResponse($error,$error["http_error"]);
        }

        // ******************************
        // ------- EXIST DISHEDS? -------
        // ******************************

         foreach($body["platos"] as $dished){

            if(DishedModel::existDishedByName($dished["name"])){
                $error = Messages::errorMessage("ERROR_REGISTER");
                $error["error_message"] = "el plato {$dished["name"]} ya esta registrado";
                return $this->sendErrorResponse($error,$error["http_error"]);
            }

        }

        // ******************************
        // ------- REGISTER DISHES ------
        // ******************************
        
        $disheds = DishedModel::createNewDisheds($body["platos"]);

        if(!$disheds){
            $error = Messages::errorMessage("ERROR_REGISTER");
            $error["error_message"] = "ocurrio un error al registrar los platos";
            return $this->sendErrorResponse($error,$error["http_error"]);
        }
        
        $message = Messages::successMessage("SUCCESS_REGISTER");
        $message["message"] = "platos registrados exitosamente";
        $message["data"] = $disheds;
        return $this->sendSuccessResponse($message);
    }

    public function updateDished(Request $request, $id){
    
        if(!is_numeric($id)) {
            $error = Messages::errorMessage('ERROR_REGISTER');
            $error["error_message"] = "el id no es valido";
            return $this->sendErrorResponse($error,$error["http_error"]);
        }

        $body = $request->all();

        $validation = Validator::make($body ?: [], [
            'name'        => "required|min:1",
            'description' => 'max:150'
        ]);

        if($validation->fails()){
            $error = Messages::errorMessage('REQUEST_BODY_FAIL');
            return $this->sendErrorResponse($error,$error["http_error"]);
        }
        
        if(DishedModel::existDishedByNameExcludeId($body["name"],$id) > 0 ){
            $error = Messages::errorMessage('ERROR_REGISTER');
            $error["error_message"] = "el nombre {$body["name"]} ya esta registrado";
            return $this->sendErrorResponse($error,$error["http_error"]);
        }
        
        $dished = DishedModel::getDishedById($id);

        if ($dished === "" or is_null($dished) or count($dished) == 0) {
            $error = Messages::errorMessage('ERROR_REGISTER');
            $error["error_message"] = "el id no existe";
            return $this->sendErrorResponse($error,$error["http_error"]);
        }

        if(DishedModel::updateDished($id,$body)){ // true or false
            $sucess = Messages::successMessage("SUCCESS_REQUEST");
            $sucess["success_message"] = "el plato se actualizo exitosamente";
            unset($sucess["data"]);
            return $this->sendSuccessResponse($sucess);
        }

    }

    public function deleteDished(Request $request, $id){
       
        $error = Messages::errorMessage('ERROR_DELETE');

        if(!is_numeric($id)) {
            $error["error_message"] = "el id no es valido";
            return $this->sendErrorResponse($error,$error["http_error"]);
        }

        if ( DishedModel::existDishedInMenu($id) > 0 ) {
            $error["error_message"] = "no se pudo eliminar, porque el plato existe dentro de un menu";
            return $this->sendErrorResponse($error,$error["http_error"]);
        }
        
        if(DishedModel::DeleteDished($id)){ // true or false
            $sucess = Messages::successMessage("SUCCESS_DELETE");
            $sucess["success_message"] = "el plato se elimino exitosamente";
            return $this->sendSuccessResponse($sucess);
        }else{
            $error["error_message"] = "el id del plato no existe";
            return $this->sendErrorResponse($error,$error["http_error"]);
        }
    }

    // =================================================
    //                 WINNER DISHED                  ||
    // ================================================ 


    public function winnerDished(Request $request, $date){
        
        if(!DatesHelper::existDate($date)) return $this->sendErrorResponse(Messages::errorMessage("DATE_NOT_EXIST"),400);
        
        $winner = DishedModel::getDishedWinner($date) ?: [];
        
        if (count($winner) < 0) return $this->sendErrorResponse(Messages::errorMessage("DATE_NOT_EXIST"),400);
        
        $success = Messages::successMessage("SUCCESS_REQUEST");
        $success["data"] = $winner;
        return $this->sendSuccessResponse($success);
    }


    // =============================================
    //                    ADMINS                  ||
    // ============================================ 

    public function getAdmins(Request $request){
        
        $user = $request->user;

        if ($user["id"] == 1) {
            $success = Messages::successMessage("SUCCESS_REQUEST");
            $success["data"] = AdminModel::all('id','username','status')->where('id','<>','1') ?: [];
            return $this->sendSuccessResponse($success);
        }

        $error = Messages::errorMessage('ERROR_ACCESS');
        $error["error_message"] = "no tienes los permisos para realizar esta accion";
        return $this->sendErrorResponse($error,$error["http_error"]);
    }

    public function createAdmin(Request $request){
        $body = $request->all();

        $validation = Validator::make($body ?: [], [
            'username' => "required|min:5|max:30",  
            'password' => "required|min:5|max:30"
        ]);

        if($validation->fails()){
            $error = Messages::errorMessage('REQUEST_BODY_FAIL');
            return $this->sendErrorResponse($error,$error["http_error"]);
        }

        if(AdminModel::existAdmin($body['username'])){
            $error = Messages::errorMessage('ERROR_REGISTER');
            $error["error_message"] = "el nombre de usuario ya existe";
            return $this->sendErrorResponse($error,$error["http_error"]);
        }

        $admin = AdminModel::createNewAdmin($body["username"],$this->encryptPassword($body["password"]));

        $success = Messages::successMessage('SUCCESS_REGISTER');
        $success["success_message"] = "usuario registrado";
        $success["data"] = $admin;
        return $this->sendSuccessResponse($success);
    }

    public function deactivateAdmin(Request $request,$id){
        
        $user = $request->user;

        if ($user["id"] != 1) {
            $error = Messages::errorMessage('ERROR_ACCESS');
            $error["error_message"] = "no tienes los permisos para realizar esta accion";
            return $this->sendErrorResponse($error,$error["http_error"]);
        }

        $error = Messages::errorMessage('ERROR_REGISTER');

        if ($id == 1) {
            $error["error_message"] = "ocurrio un error";
            return $this->sendErrorResponse($error,$error["http_error"]);
        }

        if(!AdminModel::existAdminById($id)){
            $error["error_message"] = "usuario no encontrado";
            return $this->sendErrorResponse($error,$error["http_error"]); 
        }

        if(AdminModel::deactiveAdmin($id)){
            $success = Messages::successMessage('SUCCESS_REQUEST');
            $success["success_message"] = "usuario desactivado";
            unset($success["data"]);
            return $this->sendSuccessResponse($success,200); 
        }else{
            $error["error_message"] = "ocurrio un error";
            return $this->sendErrorResponse($error,$error["http_error"]);
        }
    }

    public function activateAdmin(Request $request,$id){
        
        $user = $request->user;

        if ($user["id"] != 1) {
            $error = Messages::errorMessage('ERROR_ACCESS');
            $error["error_message"] = "no tienes los permisos para realizar esta accion";
            return $this->sendErrorResponse($error,$error["http_error"]);
        }

        if ($id == 1) {
            $error = Messages::errorMessage('ERROR_REGISTER');
            $error["error_message"] = "ocurrio un error";
            return $this->sendErrorResponse($error,$error["http_error"]);
        }

        if(!AdminModel::existAdminById($id)){
            $error = Messages::errorMessage('ERROR_REGISTER');
            $error["error_message"] = "usuario no encontrado";
            return $this->sendErrorResponse($error,$error["http_error"]);
        }

        AdminModel::activeAdmin($id);

        $success = Messages::successMessage('SUCCESS_REQUEST');
        $success["success_message"] = "usuario activado";
        unset($success["data"]);
        return $this->sendSuccessResponse($success);
    }

    public function deleteAdmin(Request $request,$id){

        $user = $request->user;

        if ($user["id"] != 1) {
            $error = Messages::errorMessage('ERROR_ACCESS');
            $error["error_message"] = "no tienes los permisos para realizar esta accion";
            return $this->sendErrorResponse($error,$error["http_error"]);
        }

        if ($id == 1) {
            $error = Messages::errorMessage('ERROR_REGISTER');
            $error["error_message"] = "ocurrio un error";
            return $this->sendErrorResponse($error,$error["http_error"]);
        }

        if(!AdminModel::existAdminById($id)){
            $error = Messages::errorMessage('ERROR_REGISTER');
            $error["error_message"] = "usuario no encontrado";
            return $this->sendErrorResponse($error,$error["http_error"]);   
        }

        AdminModel::deleteAdmin($id);

        $success = Messages::successMessage('SUCCESS_REQUEST');
        $success["success_message"] = "usuario eliminado";
        unset($success["data"]);
        return $this->sendSuccessResponse($success,200); 
        
    }

    
    // ============================================
    //                    MENUS                  ||
    // ===========================================

    public function getMenu(Request $request, $find = null){

        $message = Messages::successMessage("SUCCESS_REQUEST");

        if ($find == null){

            $message["data"] = AdminModel::getAllMenus() ?: [];
            return $this->sendSuccessResponse($message,200);
        }
        
        if (is_numeric($find)){
            $message["data"] = AdminModel::getMenuById(rtrim($find)) ?: [];
            return $this->sendSuccessResponse($message,200);
        }

        
        if ( strlen($find) < 10 or strlen($find) > 10 or !DatesHelper::existDate($find)) {
            $error = Messages::errorMessage("ERROR_DATES");
            $error["error_message"] = "la fecha no es valida";
            return $this->sendErrorResponse($error,$error["http_error"]);
        }
        
        $week = DatesHelper::getWeekByDay($find);

        $message["data"] = AdminModel::getMenuByDate($week[0][0],$week[count($week)-2][0]) ?: [];
        return $this->sendSuccessResponse($message,200);
    }
    
    public function createMenu(Request $request){
        
        $body = $request->all();

        $validation = Validator::make($body ?: [], [
            'start_date' => "required|min:10|max:10",  
            'end_date'   => "required|min:10|max:10",
            'platos'     => "required|array"
        ]);

        if($validation->fails()){
            $error = Messages::errorMessage('REQUEST_BODY_FAIL');
            return $this->sendErrorResponse($error,$error["http_error"]);
        }

        if(!DatesHelper::validateRangeDates($body)){
            $error = Messages::errorMessage('ERROR_DATES');
            return $this->sendErrorResponse($error,$error["http_error"]);
        }
       
        
        if (AdminModel::existMenuDate($body['start_date'],$body['end_date']) != 0) {
            $error = Messages::errorMessage('ERROR_REGISTER');
            $error["error_message"] = "las fechas {$body['start_date']} - {$body['end_date']} ya tienen un menu";
            return $this->sendErrorResponse($error,$error["http_error"]);
        }

        foreach($body["platos"] as $platoId) { 
            if(AdminModel::existDishedById($platoId) == 0 ){
                $error = Messages::errorMessage("ERROR_REGISTER");
                $error["error_message"] = "el plato con id {$platoId} no existe";
                return $this->sendErrorResponse($error,$error["http_error"]);
            }
        }

        $created = AdminModel::createListMenu($body);

        if(!$created){
            $message = Messages::errorMessage("ERROR_REGISTER");
            $message["message"] = "ocurrio un error al momento de registrar el menu";
            return $this->sendErrorResponse($message,500);
        }
        
        $message = Messages::successMessage("SUCCESS_REGISTER");
        $message["data"] = AdminModel::getMenuById($created);
        return $this->sendSuccessResponse($message);
    }
    
    public function deleteMenu(Request $request,$id){
        
        $error = Messages::errorMessage('ERROR_DELETE');

        if(!is_numeric($id)) {
            $error["error_message"] = "el id no es valido";
            return $this->sendErrorResponse($error,$error["http_error"]);
        }
        
        $menu = AdminModel::getMenuById($id);
    
        if(!$menu) {
            $error["error_message"] = "el menu no existe";
            return $this->sendErrorResponse($error,$error["http_error"]);
        }
        
        AdminModel::DeleteMenu($id);
        
        $success = Messages::successMessage("SUCCESS_DELETE");
        $success["success_message"] = "menu eliminado exitosamente";
        return $this->sendSuccessResponse($success);
    }
}
