<?php

namespace App\Http\Controllers;

use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\SignatureInvalidException;
use App\Http\Messages\Messages;

class AuthorizationController extends Controller
{


    static function createTokenUser(array $payload){
        return JWT::encode($payload, env('APP_KEY','DEVELOPMENT_KEY'));
    }

    static function isValidJWT(?string $jwt) :array {

        $exeptions = [
            "EXPIRED_TOKEN"   => Messages::errorMessage("EXPIRED_TOKEN"),
            "INCORRECT_TOKEN" => Messages::errorMessage("INCORRECT_TOKEN"),
            "TOKEN_NOT_FOUND" => Messages::errorMessage("TOKEN_NOT_FOUND"),
            "NO_VALID_TOKEN"  => Messages::errorMessage("INCORRECT_TOKEN")
        ];
        
        if(is_null($jwt)) return $exeptions["TOKEN_NOT_FOUND"];
    
        try {
            
            $decode = JWT::decode($jwt, env('APP_KEY','DEVELOPMENT_KEY'), array('HS256'));
            return json_decode(json_encode($decode),true);
            
        } catch (ExpiredException $e) {
        
            return $exeptions["EXPIRED_TOKEN"];
        
        }catch(\UnexpectedValueException $e){
        
            return $exeptions["NO_VALID_TOKEN"];
        
        }catch(SignatureInvalidException $e){
        
            return $exeptions["INCORRECT_TOKEN"];
        
        }catch(\DomainException $e){
         
            return $exeptions["INCORRECT_TOKEN"];
        
        }
    }
}
