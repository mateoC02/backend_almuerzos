<?php

declare( strict_types = 1 );
namespace App\Http\Controllers;

use Error;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class BaseController extends Controller
{
    function sendErrorResponse($error,int $status = 400) :JsonResponse {
        return ($status === 200)
        ? throw new Error("status must be different from 200")
        : response()->json($error,$status); 
    }

    function sendSuccessResponse($success = [],int $status = 200) :JsonResponse {
        return ($status === 400 or $status === 500)
        ? throw new Error("status must be different from 400 or 500")
        : response()->json($success,$status);
    }

    function encryptPassword(string $password) :string {
        return password_hash($password,PASSWORD_DEFAULT,["cost" => 10]);
    }

    function isTheCorrectPassword(string $passwordClient, string $userPassword) :bool {
        return password_verify($passwordClient,$userPassword);
    }

}
