<?php
declare( strict_types = 1 );

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\BaseController;
use App\Models\AdminModel;
use App\Models\EmployeeModel;

class SessionController extends BaseController{

    private $currentUser = [];

    public function index(Request $request){
        return (key_exists("username",$request->all()))
        ? $this->loginAdmin($request)
        : $this->loginEmployee($request);
    }

    protected function loginAdmin(Request $request){

        $validation = Validator::make($request->all() ?: [] ,[
            "username" => 'required|min:1|max:50',
            "password" => 'required|min:1'
        ]);

        if($validation->fails()) return $this->sendErrorResponse($validation->errors());
        
        [ "username" => $username, "password" => $password ] = $request->all();
        [ "exist"    => $exist,    "user"     => $user]      = $this->existUserAdmin($username);
        
        if (!$exist) return $this->sendErrorResponse(["error" => "userNotFound"],404);        
        
        return ($this->evaluatePasswordAndStatusUser($password,$user))
        ? $this->sendSuccessResponse($this->createTokenUser($user))
        : $this->sendErrorResponse(["error" => "userNotFound"],404);
    }

    protected function loginEmployee(Request $request){
        
        $validation = Validator::make($request->all() ?: [] ,[
            "dni"      => 'required|min:1|max:8'
        ]);

        if($validation->fails()) return $this->sendErrorResponse($validation->errors());
        
        [ "dni"   => $dni] = $request->all();
        [ "exist" => $exist, "user" => $user] = $this->existUserEmployee($dni);
        
        if (!$exist){
            $employee = $this->createEmployeeUser($dni);
            if (!$employee["success"]) return $this->sendErrorResponse(["error" => "errorRegisterUser"],500);
            $user = $employee["user"];
            
        }
        $this->setCurrentUser($user,"employee");
        return $this->sendSuccessResponse($this->createTokenUser($user));
    }

    // *************************
    //          ADMIN
    // *************************

    private function existUserAdmin($username) :array {

        $user = $this->getUserAdmin($username);

        $existValues = !(count($user) === 0);

        return  [
            "exist" => $existValues,
            "user"  => $existValues ? $user[0] : null
        ];
    }

    private function createTokenUser($user){
        
        $this->setCurrentUser($user);

        $payload = [
            'iat'  =>  time(),           // Tiempo que inició el token
            'exp'  =>  time() + (60*60), // Tiempo que expirará el token (+1 hora)
            'user' =>  $this->currentUser
        ];

        $token = AuthorizationController::createTokenUser($payload);
        $payload["token"] = $token;
        return $payload;

    }

    private function setCurrentUser($user, $type = "admin"){
        switch ($type) {
            case 'admin':
                unset($user["password"]);
                unset($user["id_role"]);
                unset($user["status"]);
                break;
        }

        $this->currentUser = $user;
    }

    private function getUserAdmin($username) :array {
        return AdminModel::select("admin.id","admin.username","admin.password","roles.role","admin.status")
        ->join("roles","admin.id_role", "=" , "roles.id")
        ->where("admin.username","=",$username)
        ->get()
        ->toArray();
    }

    private function evaluatePasswordAndStatusUser($password,$user) :bool {
        return ($this->isTheCorrectPassword($password,$user["password"]) and $user["status"] == true);
    }
    // *************************
    //          EMPLOYEE
    // *************************

    private function existUserEmployee($dni) :array {

        $employee = $this->getUserEmployee($dni);
        $existValues = !(count($employee) === 0);

        return  [
            "exist" => $existValues,
            "user"  => $existValues ? $employee[0] : null
        ];
    }

    private function getUserEmployee($dni) :array {
        return EmployeeModel::select("employee.id","employee.dni","roles.role")
        ->join("roles","employee.id_role", "=" , "roles.id")
        ->where("employee.dni","=",$dni)
        ->get()
        ->toArray();
    }

    private function createEmployeeUser($dni) :array {
        $employee = new EmployeeModel;
        $employee->dni = $dni;
        
        return ($employee->save())
        ? ["success" => true, "user" => $this->getUserEmployee($dni)[0]]
        : ["success" => false];

    }
}
