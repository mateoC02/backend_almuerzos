<?php

namespace App\Http\Middleware;

use App\Http\Controllers\AuthorizationController;
use App\Http\Messages\Messages;
use Closure;
use Illuminate\Http\Request;

class EmployeeMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $token = $request->header("Authorization");
        $authorized = AuthorizationController::isValidJWT($token);
        if (key_exists("http_error",$authorized)) return response()->json($authorized,$authorized["http_error"]);
        
        $athorized = Messages::errorMessage("ROLE_NOT_AUTHORIZATED");
        if($authorized["user"]["role"] !== "empleado") return response()->json($athorized,$athorized["http_error"]);
        
        $request->merge(['user' => $authorized["user"]]);

        return $next($request);
    }
}
