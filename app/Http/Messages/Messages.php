<?php

namespace App\Http\Messages;

class Messages{

    static function errorMessage(string $errorTitle) :array {
        return Messages::getMessageJson($errorTitle, __DIR__ . "/errorMessages.json");
    }

    static function successMessage(string $successTitle) :array {
        return Messages::getMessageJson($successTitle, __DIR__ . "/successMessages.json");
    }

    static function getMessageJson($titleMessage,$jsonPath){
        $jsonMessages = file_get_contents($jsonPath);
        $json = json_decode($jsonMessages,true);
        return $json[$titleMessage];
    }
}