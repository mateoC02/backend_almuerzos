<?php

namespace App\Helpers;

class DatesHelper {

    static function getWeekByDay($date) {

        [$year, $month, $day] = explode("-", $date);
        $dayWeek = date('l',mktime('0','0','0', $month, $day, $year));

        switch($dayWeek) {
            case 'Monday'   : $numDaysToMon = 0; break;
            case 'Tuesday'  : $numDaysToMon = 1; break;
            case 'Wednesday': $numDaysToMon = 2; break;
            case 'Thursday' : $numDaysToMon = 3; break;
            case 'Friday'   : $numDaysToMon = 4; break;
            case 'Saturday' : $numDaysToMon = 5; break;
            case 'Sunday'   : $numDaysToMon = 6; break;   
        }
    
        // Timestamp of the monday for that week
        $monday = mktime('0','0','0', $month, $day-$numDaysToMon, $year);
    
        $seconds_in_a_day = 86400;
    
        // Get date for 7 days from Monday (inclusive)
        for($i=0; $i<7; $i++)
        {
            $dates[$i][0] = date('Y-m-d',$monday+($seconds_in_a_day*$i));
        }
    
        return $dates;
    }
    
    static function getDateTomorrow($date){
        $date = new \DateTime($date);
        $date->modify('+1 day');
        return $date->format('Y-m-d');
    }

    static function getDayByDate($date) :string {
        [$year, $month, $day] = explode("-", $date);
        return date('l',mktime('0','0','0', $month, $day, $year));
    }

    static function existDate($date){
        if(!str_contains($date,"-")) return false;
        [$year,$month,$day] = explode("-", $date);
        if ($day > 31 or $month > 12 or $year < date("Y") ) return false;
        return checkdate($month,$day,$year);
    }

    static function validateRangeDates($dates) :bool {

        // ============================================================= 
        //  VALIDAMOS QUE LAS FECHAS DEL MENU SEAN VALIDAS Y EXISTAN  ||
        // ============================================================ 

    if (DatesHelper::existDate($dates["start_date"]) and DatesHelper::existDate($dates["end_date"])) {
   
        // ============================================================= 
        //  EL DIA DE INICIO Y FIN DEBEN COINCIDIR EN UNA SEMANA      ||
        //               Monday(lunes) - Sunday(domingo)             ||
        // ============================================================ 
        if( 
            DatesHelper::getDayByDate($dates["start_date"]) != "Monday"
            or
            DatesHelper::getDayByDate($dates["end_date"]) != "Saturday"
          ){  
            $start = date_create($dates["start_date"]);
            $end   = date_create($dates["end_date"]);
            $diff  = date_diff($start,$end);
            return $diff->format("%a") == 5;
        }
        
        return true;
    }
    return false;
}
}