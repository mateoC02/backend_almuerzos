<?php

declare( strict_types = 1 );

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DishedModel extends Model
{
    use HasFactory;
    protected $table = 'disheds';
    public $timestamps = false;

    public static function existDishedByName(string $nameDished) :bool {
        return count(DishedModel::where('name',$nameDished)->get()->toArray()) > 0;
    }

    public static function createNewDisheds(array $disheds) {
        $idsInsertDisheds = array_map( fn($dished) => DB::table('disheds')->insertGetId($dished) ,$disheds);
        return array_map( fn($id) => DishedModel::getDished($id),$idsInsertDisheds);
    }
    
    public static function getDished($id) :array {
        return DishedModel::where('id',$id)->get()->toArray()[0];
    }

    public static function existDishedByNameExcludeId($name,$id) :int {
        return DishedModel::where('name' , '=' , $name, 'and' , 'id' , '<>' , $id)->count();
    }

    public static function getDishedById($id){
        return DishedModel::where('id' , '=' , $id)->get()->toArray();
    }
    
    public static function updateDished($id,$body){
        $dished = DishedModel::find($id);
        $dished->name = $body["name"];
        $dished->description = $body["description"];
        return $dished->save();
    }

    public static function existDishedInMenu($id){
        return MenuDishesModel::where('id' , '=' , $id)->count();
    }

    public static function DeleteDished($id){
        return DishedModel::destroy($id);
    }

    public static function getDishedWinner($date){
        return DB::table('dish_selected_by_employee AS espm')
        ->select('espm.id_dished_menu','espm.day','plt.name','plt.description','count(espm.id_dished_menu) AS total')
        ->join('menu_dishes AS lmp','lmp.id', '=' , 'espm.id_dished_menu')
        ->join('disheds AS plt','plt.id','=','lmp.id_dished')
        ->where('espm.day','=', $date)
        ->groupBy('espm.id_dished_menu')
        ->orderByDesc('total')
        ->get()
        ->count('espm.id_dished_menu AS total');

        // return DB::select('SELECT espm.id_dished_menu, espm.day, plt.name, plt.description, count(espm.id_dished_menu) AS total FROM dish_selected_by_employee AS espm INNER JOIN menu_dishes AS lmp ON lmp.id = espm.id_dished_menu INNER JOIN disheds AS plt ON plt.id = lmp.id_dished WHERE espm.day = :date GROUP BY espm.id_dished_menu ORDER BY total DESC',["date" => $date]);
    }
}
