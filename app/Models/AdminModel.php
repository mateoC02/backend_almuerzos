<?php

declare(strict_types = 1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class AdminModel extends Model
{
    use HasFactory;
    protected $table = 'admin';
    public $timestamps = false;

    public static function existAdmin(string $username) {
        return count(AdminModel::all('username')->where('username','=',$username)->toArray());
    }

    public static function createNewAdmin($username,$password){
        $id  = DB::table('admin')->insertGetId(['username' => $username, 'password' => $password]);
        return DB::select('SELECT id, username, status FROM admin WHERE id = :id',['id' => $id])[0];
    }

    public static function existAdminById($id){
        return count(AdminModel::all('id')->where('id','=',$id)->toArray());
    }
    
    public static function deactiveAdmin($id){
        $admin = AdminModel::find($id);
        $admin->status = false;
        return $admin->save();
    }

    public static function activeAdmin($id){
        $admin = AdminModel::find($id);
        $admin->status = true;
        return $admin->save();
    }

    public static function deleteAdmin($id){
        $admin = AdminModel::find($id);
        $admin->delete();
    }

    public static function getAllMenus(){
        $menus = MenusModel::select("id","start_date","end_date")->orderByDesc("id")->get()->toArray();
        return MenusModel::getMenuByDates($menus);
    }

    public static function existMenuDate($start_date,$end_date){
        return MenusModel::where('start_date', '=' , $start_date)
        ->where('end_date', '=', $end_date )
        ->count('id');
    }

    public static function existDishedById($id){
        return DishedModel::Where('id','=',$id)->count();
    }
    
    public static function createListMenu($data){
        unset($data["user"]);
        $idMenu = AdminModel::createMenu($data);

        $disheds = array_unique($data["platos"]);
        $listMenu = [];

        for ($i=0; $i < count($disheds); $i++) { 
            $idDished = $disheds[$i];
            array_push($listMenu,["id_menu" => $idMenu, "id_dished" => $idDished]);
        }
        
        $result  = DB::table('menu_dishes')->insert($listMenu);
      
        if (!$result) return false;
        return $idMenu;
    }

    public static function createMenu($data){
        unset($data["platos"]);
        return DB::table('menus')->insertGetId($data);
    }

    public static function getMenuByDate($start,$end){
        $dates = MenusModel::select('id','start_date','end_date')->where('start_date','=', $start)->where('end_date','=',$end)->get()->toArray();
        return MenusModel::getMenuByDates($dates);
    }

    public static function getMenuById($id){
        $dates = MenusModel::select('id','start_date','end_date')->where('id','=',$id)->get()->toArray();
        if (count($dates) > 0) return MenusModel::getMenuByDates($dates);
        return false;
    }

    public static function DeleteMenu($id){
        $menu = MenusModel::find($id);
        return $menu->delete();
    }
}