<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class EmployeeModel extends Model
{
    use HasFactory;
    protected $table = 'employee';
    public $timestamps = false;

    public static function getMenuByDate($start,$end,$idEmployee){
        $dates = MenusModel::select('id','start_date','end_date')->where('start_date','=', $start)->where('end_date','=',$end)->get()->toArray();
        return EmployeeModel::getMenu($dates,$idEmployee);
    }

    public static function getMenu($datesMenu,$idUser){
        $allMenus = [];
        foreach ($datesMenu as $date) {
            $listMenu = [];
            
            $listMenu["menu_id"]    = $date['id'];
            $listMenu["start_date"] = $date['start_date'];
            $listMenu["end_date"]   = $date['end_date'];
            
            $menuId = MenusModel::select('id')->Where('start_date', '=' , $date['start_date'])->get()->toArray()[0]['id']; 
             
            $listMenu["platos"]      = DB::select("SELECT lmp.id as id_plato_menu, plt.id AS plato_id, plt.name AS nombre ,plt.description AS descripcion from menu_dishes AS lmp INNER JOIN menus AS mns ON lmp.id_menu = mns.id INNER JOIN disheds AS plt ON plt.id = lmp.id_dished WHERE start_date = :start_d and end_date = :end_d",[ "start_d" => $date['start_date'],"end_d" => $date["end_date"]]);
            $listMenu["selecteds"]   = DB::select("SELECT espm.id, espm.day AS dia, plt.name AS nombre, plt.description AS descripcion FROM dish_selected_by_employee AS espm INNER JOIN menu_dishes AS lmp ON lmp.id = espm.id_dished_menu INNER JOIN disheds AS plt ON plt.id = lmp.id_dished WHERE espm.day BETWEEN :start_date AND :end_date AND espm.id_employe = :id",["start_date" => $date['start_date'], "end_date" => $date["end_date"], "id" => $idUser]) ?: [];
            $listMenu["noSelecteds"] = DB::select("SELECT lmp.id, plt.name AS nombre, plt.description AS descripcion FROM menu_dishes AS lmp INNER JOIN disheds AS plt ON lmp.id_dished = plt.id WHERE lmp.id_menu = :id_menu AND lmp.id NOT IN (SELECT espm.id_dished_menu FROM dish_selected_by_employee AS espm INNER JOIN menu_dishes AS lmp ON lmp.id = espm.id_dished_menu INNER JOIN disheds AS plt ON plt.id = lmp.id_dished WHERE espm.day BETWEEN :start_d AND :end_date AND espm.id_employe = :id)",["start_d" => $date['start_date'], "id_menu" => $menuId, "end_date" => $date["end_date"], "id" => $idUser]) ?: [];
            
            array_push($allMenus,$listMenu);
        }
        return $allMenus;
    }

    public static function getSelectDishedMenu($date,$idEmployee){
        return DB::select("SELECT espm.id, espm.day AS dia, plt.name AS nombre, plt.description AS descripcion FROM dish_selected_by_employee AS espm INNER JOIN menu_dishes AS lmp ON lmp.id = espm.id_dished_menu INNER JOIN disheds AS plt ON plt.id = lmp.id_dished WHERE espm.day = :day AND espm.id_employe = :idEmployee ",["day" => $date,"idEmployee" => $idEmployee]);
    }

    public static function getSelectedByDate($id,$date){
        return DB::select("SELECT espm.day AS dia, plt.name AS nombre FROM dish_selected_by_employee AS espm INNER JOIN menu_dishes AS lmp ON lmp.id = espm.id_dished_menu INNER JOIN disheds AS plt ON plt.id = lmp.id_dished WHERE espm.id_employe = :id AND espm.day = :date",["id" => $id, "date" => $date]);
    }

    public static function getDateMenuByIdDishedInMenu($idDishedMenu){
        return DB::select("SELECT start_date, end_date FROM menus WHERE id = (SELECT id_menu FROM menu_dishes WHERE id = :id LIMIT 1)",["id" => $idDishedMenu]);
    }

    public static function dateIsSelected($date,$idEmployee){
        $exist = DB::select("SELECT day AS dia FROM dish_selected_by_employee WHERE day = :date AND id_employe = :idEmployee",["date" => $date,"idEmployee" => $idEmployee]);
        if (empty($exist) or is_null($exist)) return false;
        return true;
    }

    public static function selectDishedMenu($data){
        $id = DB::table('dish_selected_by_employee')->insertGetId($data);
        return DB::select("SELECT espm.id ,espm.day AS dia , plt.name AS nombre, plt.description AS descripcion FROM dish_selected_by_employee AS espm INNER JOIN menu_dishes AS lmp ON lmp.id = espm.id_dished_menu INNER JOIN disheds AS plt ON plt.id = lmp.id_dished WHERE espm.id = {$id}");
    }

}
