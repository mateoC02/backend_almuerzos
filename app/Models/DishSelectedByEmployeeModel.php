<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DishSelectedByEmployeeModel extends Model
{
    use HasFactory;
    protected $table = 'dish_selected_by_employee';
    public $timestamps = false;

}
