<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MenusModel extends Model
{
    use HasFactory;
    protected $table = 'menus';
    public $timestamps = false;

    public static function getMenuByDates($menuDates){
        
        $allMenus = [];

        foreach ($menuDates as $date) {
            $listMenu = [];
            $listMenu["menu_id"] = $date['id'];
            $listMenu["start_date"] = $date['start_date'];
            $listMenu["end_date"]   = $date['end_date'];
            
            $listMenu["disheds"] = DB::table('menu_dishes AS lmp')
            ->select('plt.id','plt.name','plt.description')
            ->join('menus AS mns','lmp.id_menu','=',"mns.id")
            ->join('disheds AS plt','lmp.id_dished','=',"plt.id")
            ->where('start_date','=',$date["start_date"])
            ->where('end_date','=',$date["end_date"])
            ->get();
            
            array_push($allMenus,$listMenu);
        }
        return $allMenus; 
    }
}
