<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MenuDishesModel extends Model
{
    use HasFactory;
    protected $table = 'menu_dishes';
    public $timestamps = false;
}
