<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\SessionController;
use Illuminate\Support\Facades\Route;

use function PHPSTORM_META\override;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', fn() => 'server up 🚀');


// =====================================================
//                    LOGIN ROUTE                     ||
// ==================================================== 

Route::post('/login',[SessionController::class,'index']); // [✔️]


// ======================================================
//                    ADMIN ROUTES                     ||
// ===================================================== 

Route::group(["middleware" => ["isAdmin","cors"], "prefix" => "admin"],function(){

    Route::get("/",[AdminController::class,'index']); // [✔️]
    
    // ***************************
    // ----- GET DASHBOARD -------
    // ***************************
    Route::get('/get/dashboard'       , [AdminController::class,'getDashboard']); // [✔️]

    
    // ****************************
    // ------- CRUD DISHEDS -------
    // ****************************
    
    Route::get ("/get/plato/{find?}"  , [AdminController::class,'getDished']);    // [✔️]
    Route::post("/create/platos"      , [AdminController::class,'createDished']); // [✔️]
    Route::put ("/update/plato/{id}"  , [AdminController::class,'updateDished']); // [✔️]
    Route::delete("/delete/plato/{id}", [AdminController::class,'deleteDished']); // [✔️]

    
    // *********************************
    // ------- GET WINNER DISHED -------
    // *********************************
    Route::get('/winner/{date}'      , [AdminController::class,'winnerDished']); // [❗]
    
    
    // ***************************
    // ------- CRUD ADMINS -------
    // ***************************
    Route::get('/get/'               , [AdminController::class,'getAdmins']);       // [✔️]
    Route::post('/create/admin'      , [AdminController::class,'createAdmin']);     // [✔️]
    Route::put ('/deactivate/{id}'   , [AdminController::class,'deactivateAdmin']); // [✔️]
    Route::put ('/activate/{id}'     , [AdminController::class,'activateAdmin']);   // [✔️]
    Route::delete('/delete/{id}'     , [AdminController::class,'deleteAdmin']);     // [✔️]

    
    // ****************************
    // ------- CRUD MENUS ---------
    // ****************************
    Route::get('/get/menu/{find?}'   , [AdminController::class,'getMenu']);     // [✔️]
    Route::post('/create/menu'       , [AdminController::class,'createMenu']);  // [✔️]
    Route::delete('/delete/menu/{id}', [AdminController::class,'deleteMenu']);  // [✔️]

});


// ======================================================
//                 EMPLOYEE ROUTES                     ||
// ===================================================== 

Route::group(["middleware" => ["isEmployee","cors"], "prefix" => "empleado"],function(){

    Route::get("/"                 , [EmployeeController::class,'index']);            // [✔️]
    Route::get('/get/menu/{date?}' , [EmployeeController::class,'getMenu']);          // [✔️]
    Route::get('/get/selecteds'    , [EmployeeController::class,'getSelecteds']);     // [✔️]
    Route::post('/select/plato'    , [EmployeeController::class,'selectDishedMenu']); // [✔️]
    
});
